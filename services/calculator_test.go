package services_test

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/sergiolucena1/test_go/services"
	"testing"
)

func TestSum(t *testing.T){
	//if services.Sum(2, 2) != 4 {
	//	t.Errorf("2 + 2 must be 4")
	//}
	require.Equal(t, services.Sum(2,2),4)
}

func TestMult(t *testing.T){
	//if services.Multply(2, 2) != 4 {
	//	t.Errorf("2 * 2 must be 4")
	//}
	require.Equal(t, services.Multply(2,2),4)
}
